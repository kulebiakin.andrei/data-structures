import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CustomArrayListTest {

    @Test
    void testInitialCapacity() {
        // when
        int initialCapacity = 25;
        CustomArrayList list = new CustomArrayList(initialCapacity);

        // then
        assertThat(list.getCapacity()).isEqualTo(initialCapacity);
    }

    @Test
    void testDefaultCapacity() {
        // when
        CustomArrayList list = new CustomArrayList();

        // then
        assertThat(list.getCapacity()).isEqualTo(10);
    }

    @Test
    void testAddAndGet() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        CustomArrayList list = new CustomArrayList();

        // when
        fillCustomList(list, elements);

        // then
        assertThat(list.getSize()).isEqualTo(elements.length);
        assertThat(list.getCapacity()).isEqualTo(20);

        SoftAssertions softAssertions = new SoftAssertions();
        for (int i = 0; i < elements.length; i++) {
            softAssertions.assertThat(list.get(i)).isEqualTo(elements[i]);
        }
        softAssertions.assertAll();
    }

    @Test
    void testAddByIndexAndGet() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CustomArrayList list = new CustomArrayList();
        fillCustomList(list, elements);

        // when
        int index = 2;
        int indexValue = 42;
        list.add(indexValue, index);

        // then
        assertThat(list.getSize()).isEqualTo(elements.length + 1);
        assertThat(list.getCapacity()).isEqualTo(20);

        SoftAssertions softAssertions = new SoftAssertions();
        for (int i = 0; i < index; i++) {
            softAssertions.assertThat(list.get(i)).isEqualTo(elements[i]);
        }
        softAssertions.assertThat(list.get(index)).isEqualTo(indexValue);
        for (int i = index + 1; i < elements.length + 1; i++) {
            softAssertions.assertThat(list.get(i)).isEqualTo(elements[i - 1]);
        }
        softAssertions.assertAll();
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, Integer.MAX_VALUE})
    void testAddByIncorrectIndex(int incorrectIndex) {
        // given
        CustomArrayList list = new CustomArrayList();

        // then
        assertThatThrownBy(() -> list.add(42, incorrectIndex))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, Integer.MAX_VALUE})
    void testGetByIncorrectIndex(int incorrectIndex) {
        // given
        CustomArrayList list = new CustomArrayList();

        // then
        assertThatThrownBy(() -> list.get(incorrectIndex))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @Test
    void testRemoveByIndex() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        CustomArrayList list = new CustomArrayList();
        fillCustomList(list, elements);

        // when
        int indexToRemove = 2;
        list.removeByIndex(indexToRemove);

        // then
        assertThat(list.getSize()).isEqualTo(elements.length - 1);
        assertThat(list.getCapacity()).isEqualTo(20);

        SoftAssertions softAssertions = new SoftAssertions();
        for (int i = 0; i < indexToRemove; i++) {
            softAssertions.assertThat(list.get(i)).isEqualTo(elements[i]);
        }
        for (int i = indexToRemove + 1; i < elements.length - 1; i++) {
            softAssertions.assertThat(list.get(i - 1)).isEqualTo(elements[i]);
        }
        softAssertions.assertAll();
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, Integer.MAX_VALUE})
    void testRemoveByIncorrectIndex(int incorrectIndex) {
        // given
        CustomArrayList list = new CustomArrayList();

        // then
        assertThatThrownBy(() -> list.removeByIndex(incorrectIndex))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }


    @Test
    void testRemoveFirstByValue() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CustomArrayList list = new CustomArrayList();
        fillCustomList(list, elements);

        // when
        int indexToRemove = 2;
        list.removeFirstByValue(elements[indexToRemove]);

        // then
        assertThat(list.getSize()).isEqualTo(elements.length - 1);
        assertThat(list.getCapacity()).isEqualTo(10);

        SoftAssertions softAssertions = new SoftAssertions();
        for (int i = 0; i < indexToRemove; i++) {
            softAssertions.assertThat(list.get(i)).isEqualTo(elements[i]);
        }
        for (int i = indexToRemove + 1; i < elements.length - 1; i++) {
            softAssertions.assertThat(list.get(i - 1)).isEqualTo(elements[i]);
        }
        softAssertions.assertAll();
    }

    @Test
    void testReplaceByIndex() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CustomArrayList list = new CustomArrayList();
        fillCustomList(list, elements);

        // when
        int index = 2;
        int value = 42;
        list.replaceByIndex(index, value);

        // then
        assertThat(list.get(index)).isEqualTo(value);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, Integer.MAX_VALUE})
    void testReplaceByIncorrectIndex(int incorrectIndex) {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        CustomArrayList list = new CustomArrayList();
        fillCustomList(list, elements);

        // then
        assertThatThrownBy(() -> list.replaceByIndex(incorrectIndex, 42))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @Test
    void testToString() {
        // given
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        CustomArrayList list = new CustomArrayList();

        // when
        fillCustomList(list, elements);

        // then
        assertThat(list.toString()).isEqualTo("[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]");
    }

    @Test
    void testToStringEmptyList() {
        // given
        CustomArrayList list = new CustomArrayList();

        // then
        assertThat(list.toString()).isEqualTo("[]");
    }

    private void fillCustomList(CustomArrayList list, int[] elements) {
        for (int element : elements) {
            list.add(element);
        }
    }
}
