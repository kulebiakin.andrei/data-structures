public class CustomArrayList {

    private static final int DEFAULT_SIZE = 10;

    private int[] elements;

    private int currentIndex;

    public CustomArrayList() {
        elements = new int[DEFAULT_SIZE];
    }

    public CustomArrayList(int initialCapacity) {
        if (initialCapacity > 0) {
            elements = new int[initialCapacity];
        } else if (initialCapacity == 0) {
            elements = new int[DEFAULT_SIZE];
        } else {
            throw new IllegalArgumentException("Initial capacity must be >= 0");
        }
    }

    public void add(int element) {
        if (currentIndex >= elements.length) {
            doubleArraySize();
        }
        elements[currentIndex] = element;
        currentIndex++;
    }

    public void add(int element, int index) {
        checkIndex(index);
        if (currentIndex >= elements.length) {
            doubleArraySize();
        }
        System.arraycopy(elements, index, elements, index + 1, elements.length - currentIndex);
        elements[index] = element;
        currentIndex++;
    }

    public int get(int index) {
        checkIndex(index);
        return elements[index];
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("Current size: " + elements.length + ", index: " + index);
        }
    }

    private void doubleArraySize() {
        int newSize = elements.length * 2;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }

    public void removeByIndex(int index) {
        checkIndex(index);
        System.arraycopy(elements, index + 1, elements, index, elements.length - index - 1);
        currentIndex--;
    }

    public void removeFirstByValue(int value) {
        int indexToRemove = -1;
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == value) {
                indexToRemove = i;
            }
        }

        if (indexToRemove >= 0) {
            System.arraycopy(elements, indexToRemove + 1, elements, indexToRemove, elements.length - indexToRemove - 1);
            currentIndex--;
        }
    }

    public void replaceByIndex(int index, int element) {
        checkIndex(index);
        elements[index] = element;
    }

    public int getSize() {
        return currentIndex;
    }

    public int getCapacity() {
        return elements.length;
    }

    @Override
    public String toString() {
        if (currentIndex <= 0) {
            return "[]";
        }

        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (int i = 0; i < currentIndex; i++) {
            builder.append(elements[i]);
            if (i < currentIndex - 1) {
                builder.append(", ");
            } else {
                builder.append("]");
            }
        }
        return builder.toString();
    }
}
