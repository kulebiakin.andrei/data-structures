public class CustomLinkedList {

    private Node head;
    private Node tail;
    private int size;

    public CustomLinkedList() {
    }

    public void add(int value) {
        Node newNode = new Node(value, null, tail);
        if (tail != null) {
            tail.next = newNode;
        } else {
            head = newNode;
        }
        tail = newNode;
        size++;
    }

    public void add(int value, int index) {
        checkIndex(index);
        if (index == size) {
            add(value);
        }

        Node currentNode = getNode(index);
        Node previous = currentNode.previous;
        Node newNode = new Node(value, currentNode, previous);
        currentNode.previous = newNode;
        if (previous != null) {
            previous.next = newNode;
        } else {
            head = newNode;
        }
        size++;
    }

    public int get(int index) {
        checkIndex(index);
        Node node = getNode(index);
        return node.value;
    }

    public void removeByIndex(int index) {
        checkIndex(index);
        Node node = getNode(index);
        removeNode(node);
    }

    public void removeFirstByValue(int value) {
        for (Node node = head; node != null; node = node.next) {
            if (node.value == value) {
                removeNode(node);
            }
        }
    }

    private void removeNode(Node node) {
        Node previous = node.previous;
        Node next = node.next;

        if (previous != null) {
            previous.next = next;
            node.previous = null;
        } else {
            head = next;
        }

        if (next != null) {
            next.previous = previous;
            node.next = null;
        } else {
            tail = previous;
        }
        size--;
    }

    public void replaceByIndex(int index, int value) {
        checkIndex(index);
        Node node = getNode(index);
        node.value = value;
    }

    private void checkIndex(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Current size: " + size + ", index: " + index);
        }
    }

    private Node getNode(int index) {
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        if (size <= 0) {
            return "[]";
        }

        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (Node node = head; node != null; node = node.next) {
            builder.append(node.value);
            if (node != tail) {
                builder.append(", ");
            } else {
                builder.append("]");
            }
        }
        return builder.toString();
    }

    private static class Node {
        private int value;
        private Node next;
        private Node previous;

        public Node(int value, Node next, Node previous) {
            this.value = value;
            this.next = next;
            this.previous = previous;
        }
    }
}
